from telegram.ext import Updater, CallbackContext
from telegram.ext import CommandHandler, CallbackQueryHandler, InlineQueryHandler

from telegram import Update
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

from telegram import InlineQueryResultArticle, InputTextMessageContent

from decouple import config
from uuid import uuid4


def start(update: Update, context: CallbackContext):
    markup = InlineKeyboardMarkup(
        [
            [InlineKeyboardButton('CallBackData', callback_data="Hello, World")],
            [InlineKeyboardButton('InvalidCallBackData', callback_data=";iefnaeubf")],
            [InlineKeyboardButton('Switch2InlineQuery (Choose the chat )', switch_inline_query="Hello")],
            [InlineKeyboardButton('Switch2InlineQuery ( current chat )', switch_inline_query_current_chat="Hello")]
        ]
    )
    update.effective_message.reply_text('به قسمت چهارم از ساخت ربات تلگرام خوش آمدید', reply_markup=markup)


def randomMsg(update, context):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id, "Hello")


def callBackQuery(update: Update, context: CallbackContext): 
    data = update.callback_query.data
    if data == "Hello, World":
        update.callback_query.answer("this is the callBackQuery answer")
    else:
        print('Invalid CallBackData')
        randomMsg(update, context)
    


def inlineQuery(update: Update, context: CallbackContext): 
    data = update.inline_query.query
    if data == "Hello":
        result = [
            InlineQueryResultArticle(
                id = uuid4(),
                title = "Article Title 1",
                input_message_content=InputTextMessageContent("it is the first response."),
                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                thumb_url="https://duckduckgo.com/i/5d7f2f94.jpg"
            ),
            InlineQueryResultArticle(
                id = uuid4(),
                title = "Article Title 1",
                input_message_content=InputTextMessageContent("it is the first response."),
                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                thumb_url="https://duckduckgo.com/i/5d7f2f94.jpg"
            ),
        ]
    
    else:
        result = [
            InlineQueryResultArticle(
                id = uuid4(),
                title = "Article Title 2",
                input_message_content=InputTextMessageContent("it is the second response."),
                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                thumb_url="https://duckduckgo.com/i/5d7f2f94.jpg"
            )
        ]

    update.inline_query.answer(result, cache_time=5)
    # context.bot.answer_inline_query(update.inline_query.id, )


def main():
    API_TOKEN = config('API_TOKEN')
    bot = Updater(API_TOKEN)
    dis = bot.dispatcher


    dis.add_handler(CommandHandler('start', start))
    dis.add_handler(CallbackQueryHandler(callBackQuery))
    dis.add_handler(InlineQueryHandler(inlineQuery))

    bot.start_polling()
    bot.idle()

if __name__=="__main__":
    main()