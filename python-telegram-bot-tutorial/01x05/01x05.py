from telegram import Update

from telegram.ext import Updater, CallbackContext
from telegram.ext import CommandHandler, Filters

from telegram.utils import helpers

from decouple import config 


OFFER = "hello"
VIP = "vip"


def deepLink(update, context):
    update.message.reply_text("This is VIP section")

def start(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    text =  f"به قسمت پنجم از ساخت ربات تلگرام خوش آمدید"
    bot_username = context.bot.username

    try:
        arg = context.args[0]
    except:
        arg = ""

    text += f"\n\n{arg=}"

    url = helpers.create_deep_linked_url(bot_username, VIP)
    text += f"\n\n{url}"


    context.bot.send_message(chat_id, text)



def main():

    API_TOKEN = config('API_TOKEN')
    bot = Updater(API_TOKEN)
    dis = bot.dispatcher

    dis.add_handler(CommandHandler('start', start))
    dis.add_handler(CommandHandler('start', deepLink, Filters.regex(VIP)))

    print(dis.handlers)

    print('Bot is Running...')
    bot.start_polling()
    bot.idle()

if __name__=='__main__':
    main()