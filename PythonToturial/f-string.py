from math import pi

def largeNumber(num: int):

    num = str(num)[::-1]
    text = ""

    for i in range(0, len(num), 3):
        text += num[i:i+3] + ","

    if text[-1] == ",": text = text[:-1]

    return text[::-1]


name = 'Ali'
age  = 23

# string = "Hello, my name is %s and I'm %i years old"%(name, age)
# string = "Hello, my name is {1} and I'm {0} years old".format(name, age)

# string = f"Hello, my name is {name} and I'm {age} years old"

def string():
    return "Hello"

def loop():
    for i in range(1001):
        print(f"{i:03d}")

condition = False

if __name__=="__main__":
    # print(largeNumber(2**64))
    # print(f"{2_675_249:,}")

    # loop()
    print(f"{pi:.5f}")
    print(f"{string() if condition else 'not found'}")
